import java.util.Random;
import java.util.Scanner;



public class rock_paper_scissors {
    static int win = 0;
    static int draw = 0;
    static int lose = 0;
    public static int computers_turn(){
        Random random = new Random();
        int choice = random.nextInt(1, 4);
        System.out.println("computer's choice - "+number_to_word(choice));
        return choice;
    }
    public static int person_turn(Scanner scanner){
        System.out.println("1 - rock, 2 - paper, 3 - scissors");
        System.out.println("to stop game enter '0'");
        return scanner.nextInt();
    }
    public static int winner_loser(int person, int computer){
        if(computer == person) {
            System.out.println("draw");
            return 1;
        }
        else if((computer == 1 && person == 2) || (computer == 2 && person == 3) || (computer == 3 && person == 1)){
            System.out.println("you win!");
            return 2;
        }
        System.out.println("you lose!");
        return 3;
    }
    public static void counter(int who_win){
        switch (who_win) {
            case 1 -> draw++;
            case 2 -> win++;
            case 3 -> lose++;
        }
    }
    public static String number_to_word(int num){
        return switch (num) {
            case 1 -> "rock";
            case 2 -> "paper";
            case 3 -> "scissors";
            default -> "";
        };
    }
    public static boolean is_correct(int person) {
        if (person < 0 || person > 3) {
            System.out.println("incorrect number! Write again: ");
            return false;
        }
        return true;
    }
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        boolean end = false;
        while(!end){
            int person = person_turn(scanner);

            while(!is_correct(person)){
              person = person_turn(scanner);
            }

            if(person == 0) {
                end = true;
                return;
            }

            System.out.println("your choice - "+number_to_word(person));

            int who_win = winner_loser(person,computers_turn());

            counter(who_win);

            System.out.println("win: "+win+", lose: "+lose+", draw: "+draw);
        }
    }
}
