import java.util.Scanner;

public class Renju {
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_RED = "\u001B[31m";

    public static void cleanTable(char[][] table){
        for(int i = 1; i < table.length; i++){
            for(int j = 1; j < table[i].length; j++){
                table[i][j] = '_';
            }
        }
    }

    public static void showTable(char[][] table){
        for (int i = 0; i < table.length; i++) {
            for (int j = 0; j < table[i].length; j++) {
                if(table[i][j] == '0'){
                    System.out.print(ANSI_YELLOW + "0" + ANSI_RESET);
                }
                else if(table[i][j] == '#' && i != 0 && j != 0){
                    System.out.print(ANSI_RED + "0" + ANSI_RESET);
                }
                else{
                    System.out.print(table[i][j]);
                }
            }
            System.out.println();
        }
    }

    public static void turn(Scanner scanner, char[][] table, int player){
        System.out.println("Enter your turn(first row, second column, ex. ae):");
        String coordinates = scanner.nextLine();

        while(coordinates.charAt(0) > 'o' || coordinates.charAt(1) > 'o' || coordinates.charAt(0) < 'a'|| coordinates.charAt(1) < 'a'){
            System.out.println("Incorrect coordinates, please write again:");
            coordinates = scanner.nextLine();
        }

        for(int i = 1; i < table.length; i++){
            for(int j = 1; j < table[i].length; j++){
                if(coordinates.charAt(0) == table[0][i] && coordinates.charAt(1) == table[j][0]){
                    if(table[i][j] == '0' || table[i][j] == '#'){
                        System.out.println("These position is busy, write another:");
                        coordinates = scanner.nextLine();

                        while(coordinates.charAt(0) > 'o' || coordinates.charAt(1) > 'o' || coordinates.charAt(0) < 'a'|| coordinates.charAt(1) < 'a'){
                            System.out.println("Incorrect coordinates, please write again:");
                            coordinates = scanner.nextLine();
                        }

                        i = 0; j = 0;
                    }
                }
            }
        }

        for(int i = 1; i < table.length; i++){
            for(int j = 1; j < table[i].length; j++){
                if(coordinates.charAt(0) == table[0][i] && coordinates.charAt(1) == table[j][0]){
                    if(player == 1){
                        table[i][j] = '0';
                    }
                    else{
                        table[i][j] = '#';
                    }
                }
            }
        }
    }

    public static int isWin(char[][] table){
        for(int i = 1; i < table.length; i++){
            for(int j = 1; j < table[i].length - 4; j++){
                if(table[i][j] == '0' && table[i][j+1] == '0' && table[i][j+2] == '0' && table[i][j+3] == '0' && table[i][j+4] == '0'){
                    return 1;
                }
                else if(table[i][j] == '#' && table[i][j+1] == '#' && table[i][j+2] == '#' && table[i][j+3] == '#' && table[i][j+4] == '#'){
                    return 2;
                }
            }
        }
        for(int i = 1; i < table.length - 4; i++){
            for(int j = 1; j < table[i].length; j++){
                if(table[i][j] == '0' && table[i+1][j] == '0' && table[i+2][j] == '0' && table[i+3][j] == '0' && table[i+4][j] == '0'){
                    return 1;
                }
                else if(table[i][j] == '#' && table[i+1][j] == '#' && table[i+2][j] == '#' && table[i+3][j] == '#' && table[i+4][j] == '#'){
                    return 2;
                }
            }
        }
        for(int i = 1; i < table.length - 4; i++){
            for(int j = 1; j < table[i].length-4; j++){
                if(table[i][j] == '0' && table[i+1][j+1] == '0' && table[i+2][j+2] == '0' && table[i+3][j+3] == '0' && table[i+4][j+4] == '0'){
                    return 1;
                }
                else if(table[i][j] == '#' && table[i+1][j+1] == '#' && table[i+2][j+2] == '#' && table[i+3][j+3] == '#' && table[i+4][j+4] == '#'){
                    return 2;
                }
            }
        }
        return 3;
    }

    public static boolean isSpace(char[][] table){
        int count = 0;
        for(int i = 1; i < table.length; i++){
            for(int j = 1; j < table[i].length; j++){
                if(table[i][j] == '_'){
                    count++;
                }
            }
        }
        return count != 0;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        char[][] table = new char[16][16];

        table[0][0] = '#';table[0][1] = 'a';table[0][2] = 'b';table[0][3] = 'c';table[0][4] = 'd';table[0][5] = 'e';table[0][6] = 'f';table[0][7] = 'g';table[0][8] = 'h';table[0][9] = 'i';table[0][10] = 'j';table[0][11] = 'k';table[0][12] = 'l';table[0][13] = 'm';table[0][14] = 'n';table[0][15] = 'o';
        table[1][0] = 'a';table[2][0] = 'b';table[3][0] = 'c';table[4][0] = 'd';table[5][0] = 'e';table[6][0] = 'f';table[7][0] = 'g';table[8][0] = 'h';table[9][0] = 'i';table[10][0] = 'j';table[11][0] = 'k';table[12][0] = 'l';table[13][0] = 'm';table[14][0] = 'n';table[15][0] = 'o';

        cleanTable(table);

        System.out.print("Enter name of first player: ");
        String name1 = scanner.nextLine();
        System.out.print("Enter name of second player: ");
        String name2 = scanner.nextLine();

        showTable(table);

        while(true){
            System.out.println(name1+"'s turn:");
            turn(scanner, table, 1);
            showTable(table);

            if(isWin(table) == 1){
                System.out.println(name1+" win!!!");
                System.out.println("ENDGAME");
                break;
            }

            System.out.println(name2+"'s turn:");
            turn(scanner, table, 2);
            showTable(table);

            if(isWin(table) == 2){
                System.out.println(name2+" win!!!");
                System.out.println("ENDGAME");
                break;
            }

            if(!isSpace(table)){
                System.out.println("DRAW!!!");
                break;
            }
        }
    }
}
